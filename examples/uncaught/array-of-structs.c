#include <stdlib.h>

typedef struct {
    int x[10];
} s;

int main(void) {
    s structArray[10];
    structArray[0].x[11] = 5; 

    s *heapArray = malloc(sizeof(s) * 10);
    heapArray[5].x[11] = 5;
    free(heapArray);
}
