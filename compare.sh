#! /bin/sh

# Arguments are files to compile
# Output is a comparison of Valgrind vs ASan output

if [ "$#" -eq 0 ]; then
    echo "Needs at least one argument"
    exit 1
fi

tempname=`mktemp`

catch_errors() {
    if [ "$1" -eq 0 ]; then
        echo "No errors caught."
    else
        echo "Errors. Oh no!"
    fi
}


echo "Compiling and running with AddressSanitizer"
echo "==========================================="
echo ""
export ASAN_OPTIONS="detect_stack_use_after_return=1:detect_leaks=1"
clang -g -fsanitize=address "$@" -o "$tempname" 
"$tempname"
rm "$tempname"


echo ""
echo "Compiling and running with Valgrind"
echo "==========================================="
echo ""
clang -g "$@" -o "$tempname" 
valgrind --error-exitcode=2 "$tempname"
rm "$tempname"
