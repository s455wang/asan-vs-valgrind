#include <stdlib.h>

int main(void) {
    int *a = malloc(sizeof(int) * 4);
    int *b = malloc(sizeof(int) * 1000);
    a[5] = 3;
    a[500] = 3; // lands somewhere in B
    free(a);
    free(b);
}
