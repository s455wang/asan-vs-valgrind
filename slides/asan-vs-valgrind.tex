\batchmode
\documentclass[xcolor=table]{beamer}
\usepackage{hyperref}
\usepackage[backend=bibtex, style=numeric]{biblatex}   % bibliography
\usepackage[T1]{fontenc}

\usepackage{minted}
\setminted{frame=lines, fontsize=\footnotesize, linenos}
%\usetheme{AnnArbor}
%\usetheme{Antibes}
%\usetheme{Bergen}
%\usetheme{Berkeley}
%\usetheme{Berlin}
%\usetheme{Boadilla}
%\usetheme{boxes}
%\usetheme{CambridgeUS}
%\usetheme{Copenhagen}
%\usetheme{Darmstadt}
\usetheme{default}
%\usetheme{Frankfurt}
%\usetheme{Goettingen}
%\usetheme{Hannover}
%\usetheme{Ilmenau}
%\usetheme{JuanLesPins}
%\usetheme{Luebeck}
%\usetheme{Madrid}
%\usetheme{Malmoe}
%\usetheme{Marburg}
%\usetheme{Montpellier}
%\usetheme{PaloAlto}
%\usetheme{Pittsburgh}
%\usetheme{Rochester}
%\usetheme{Singapore}
%\usetheme{Szeged}
%\usetheme{Warsaw}


\title{AddressSanitizer and Valgrind in CS 136}
%\subtitle{}

\author{Charlie Wang}
\institute[] % (optional, but mostly needed)
{
  \inst{}%
  Instructional Support Group \\
  Cheriton School of Computer Science\\
  University of Waterloo 
}
% - Use the \inst command only if there are several affiliations.
% - Keep it simple, no one is interested in your street address.

\date{November 16, 2015}
% - Either use conference name or its abbreviation.
% - Not really informative to the audience, more for people (including
%   yourself) who are reading the slides online

%\subject{Theoretical Computer Science}
% This is only inserted into the PDF information catalog. Can be left
% out. 

% If you have a file called "university-logo-filename.xxx", where xxx
% is a graphic format that can be processed by latex or pdflatex,
% resp., then you can add a logo as follows:

% \pgfdeclareimage[height=0.5cm]{university-logo}{university-logo-filename}
% \logo{\pgfuseimage{university-logo}}

% Delete this, if you do not want the table of contents to pop up at
% the beginning of each subsection:
\AtBeginSubsection[]
{
%  \begin{frame}<beamer>{Outline}
%    \tableofcontents[currentsection,currentsubsection]
%  \end{frame}
}
% Let's get started

\bibliography{references}
\begin{document}

\begin{frame}
  \titlepage
\end{frame}
\begin{frame}{Outline}
    \tableofcontents
\end{frame}

\section{Introduction}
\subsection{Motivation}

\begin{frame}{Why do we want memory checking?}{}
  \begin{itemize}
  \item {
    C is hard.
  }
  \item {
    Students have little experience with raw pointers and memory.
  }
  \item {
    C only segfaults when you're lucky.
  }
  \item {
    No one tells you about memory leaks.
  }
  \end{itemize}
\end{frame}

\subsection{Current Status}

\begin{frame}{AddressSanitizer}
    Available in clang 3.1 and later. Seashell currently uses clang 3.6.
  \begin{itemize}
  \item {Compiler adds checks to code for various memory errors}
  \item {Memory overhead factor of 3.4 \cite{blah} }
  \item {Run time overhead of 3x \cite{blah}}
  \end{itemize}
\end{frame}


\begin{frame}{Valgrind}
    ``Valgrind'' typically refers to ``Valgrind with Memcheck'', a tool that checks for memory errors. Valgrind is a platform for creating dynamic binary instrumentation tools (DBI).
    \begin{itemize}
    \item {Basically runs compiled code in a VM (works on compiled binaries).}
    \item {Some memory overhead: 1.5GB memory usage exhausts address space \cite{nethercote2007valgrind}}
    \item {Slowdown of 5-100x \cite{valgrindabout}}
    \item {Mean slowdown factor of 22.2x \cite{nethercote2007valgrind}}
    \end{itemize}
\end{frame}

\section{Comparison}

\subsection{Glossary of Terms}
\begin{frame}{Glossary}
    \begin{description}
        \item[Use-after-return] Returning an address to an old stack frame and using it.
        \item[Use-after-free] Using heap memory that has been freed.
        \item[Stack-buffer-overflow] Going past the bounds of an array on the stack.
        \item[Heap-buffer-overflow] ---\textquotedbl--- heap.
        \item[Global-buffer-overflow] ---\textquotedbl--- globals.
        \item[Uninitialized read] Reading from uninitialized memory, anywhere.
        \item[Direct memory leak] Losing a pointer to heap memory.
        \item[Indirect memory leak] Leaked memory that never had a direct stack pointer to it. Eg. linked lists.
        \item[Still reachable] Not freed, but stored in a global somewhere.
    \end{description}
\end{frame}

\subsection{Use-after-return}
\begin{frame}{Use-after-return}
Students often do this by creating a stack array, assigning a pointer in a struct to point to it, and returning the struct by value.    
\inputminted{c}{"../examples/caught/use-after-return.c"}
\end{frame}

\begin{frame}{Use-after-return}
    \begin{block}{ASan}
        Stack frames are literally put into the heap. Printing out addresses confirms this. Protection is disabled by default, but can be enabled by an environment variable. 
    \end{block}
    \begin{block}{Valgrind}
        Valgrind does \alert{not} catch this.
\end{block}
\end{frame}

\subsection{Use-after-free}
\begin{frame}{Use-after-free}
    \inputminted{c}{"../examples/caught/use-after-free.c"}
    \begin{block}{ASan}
        Freed heap blocks are put into a quarantine FIFO queue. If enough memory is allocated and de-allocated, previously-freed memory could be re-allocated, allowing an indirect use-after-free \cite[p. 5]{blah}. This is highly unlikely to happen in CS 136.
    \end{block}
    \begin{block}{Valgrind}
        Valgrind catches this reliably.
    \end{block}
\end{frame}

\subsection{Stack-buffer-overflow}

\begin{frame}{Stack-buffer-overflow}
    \inputminted{c}{"../examples/caught/stack-buffer.c"}
    \begin{block}{ASan}
        This error is caught reliably. This error is \alert{not} caught in the case of an array of structs, where overflow in one struct will cause it to access the array in the next struct.
    \end{block}
    \begin{block}{Valgrind}
        Valgrind's Memcheck does \alert{not} check stack memory.
    \end{block}
\end{frame}

\subsection{Heap-buffer-overflow}
\begin{frame}{Heap-buffer-overflow}
    \inputminted{c}{"../examples/caught/heap-buffer.c"}
    Both tools catch this reliably, \alert{except} when the overflow skips over invalid memoy and accesses valid memory in an adjacent block. 
    
    The previous point about overflowing an array in a struct inside an array of structs applies here as well.
\end{frame}

\subsection{Global-buffer-overflow}
\begin{frame}{Global-buffer-overflow}
    \inputminted{c}{"../examples/uncaught/global-buffer.c"}
    ASan is supposed to catch this \cite{blah} but it currently does \alert{not}.

    Valgrind's Memcheck does \alert{not} check this.
\end{frame}

\subsection{Uninitialized memory}
\begin{frame}{Uninitialized memory}
    \inputminted{c}{"../examples/caught/uninitialized.c"}
    \begin{block}{ASan}
        ASan does \alert{not} catch this, although clang itself can catch simple uninitialized memory accesses at compile time.
    \end{block}
    \begin{block}{Valgrind}
        Valgrind catches this in a mostly reliable fashion. 
    \end{block}
\end{frame}

\subsection{Direct memory leak}
\begin{frame}{Direct memory leak}
    \inputminted{c}{"../examples/caught/direct-leak.c"}
    Both tools reliably catch this error.
\end{frame}

\subsection{Indirect memory leak}
\begin{frame}{Indirect memory leak}
    \inputminted{c}{"../examples/caught/indirect-leak.c"}
    Both tools reliably catch this error. The direct memory leak here is from losing \mintinline{c}{p}, whereas the indirect memory leak is from losing \mintinline{c}{p->a}.
\end{frame}

\subsection{Still reachable}
\begin{frame}{Still reachable}
    \inputminted{c}{"../examples/caught/still-reachable.c"}
    \begin{block}{ASan}
        If a block of heap memory is still reachable through a global pointer after the program ends, ASan will \alert{not} report anything. ASan only reports when all pointers are lost. 
    \end{block}
    \begin{block}{Valgrind}
        Valgrind has a distinct message for this case and will report ``still reachable'' even if the pointer is not lost. The C++ standard library causes false positives with this. When the pointer is actually lost, it turns into a leak.
    \end{block}
\end{frame}

\section{Conclusion}

\subsection{Summary}
\begin{frame}{Summary}
\begin{table}
\centering
\caption{Summary of results for ASan vs Valgrind}
\label{my-label}
\begin{tabular}{l|l|l}
\textbf{Error Type}    & \textbf{ASan}               & \textbf{Valgrind}           \\ \hline
Use-after-return       & {\color[HTML]{036400} Yes}  & {\color[HTML]{CB0000} No}   \\
Use-after-free         & {\color[HTML]{036400} Yes}  & {\color[HTML]{036400} Yes}  \\
Stack-buffer-overflow  & {\color[HTML]{036400} Yes}  & {\color[HTML]{CB0000} No}   \\
Heap-buffer-overflow   & {\color[HTML]{036400} Yes}  & {\color[HTML]{036400} Yes}  \\
Global-buffer-overflow & {\color[HTML]{CB0000} No}   & {\color[HTML]{CB0000} No}   \\
Uninitialized read     & {\color[HTML]{CB0000} No}   & {\color[HTML]{036400} Yes}  \\
Direct memory leak     & {\color[HTML]{036400} Yes}  & {\color[HTML]{036400} Yes}  \\
Indirect memory leak   & {\color[HTML]{036400} Yes}  & {\color[HTML]{036400} Yes}  \\
Still-reachable        & {\color[HTML]{036400} Yes*} & {\color[HTML]{036400} Yes*}
\end{tabular}
\end{table} 
\end{frame}

\subsection{Recommendation}
\begin{frame}{Recommendation}
    Valgrind's Memcheck does not catch stack errors. An experimental tool, SGCheck, claims to catch stack and global memory errors but it's been broken since at least 2012.\\

    LLVM claims to be working on a MemorySanitizer tool, to be used as a complement to AddressSanitizer. It is supposed to catch uninitialized memory errors \cite{msan}, but I haven't tested it (I didn't have time to compile clang 3.8 from source). \\

    My conclusion is to stay on AddressSanitizer, because Valgrind does not catch stack errors. Students do not use the heap until very late in the course. 
\end{frame}

\section{Appendix}
\begin{frame}{Notes}
    \begin{itemize}
        \item ASan instrumentation can be disabled with \mintinline{c}{ __attribute__((no_sanitize("address")))} 
        \item ASan output needs to be parsed. The project is against having structured (JSON, XML) output in principle \cite{nostructasan}.
        \item Valgrind can output error messages in XML.
        \item Source code for all samples and these slides can be found at \url{https://git.uwaterloo.ca/s455wang/asan-vs-valgrind}
        \item The different categories of errors were taken from \url{https://github.com/google/sanitizers/wiki/AddressSanitizerComparisonOfMemoryTools}
    \end{itemize}
\end{frame}

\begin{frame}[t,allowframebreaks]
    {
    \renewcommand*{\bibfont}{\scriptsize}
    \sloppy
     \printbibliography
    }
\end{frame}


\end{document}


