#include <stdio.h>

int foo(int a) {
   return a; 
}

int main(void) {
    int a;
    printf("%d\n", a); // nothing is caught unless we printf
    int b = foo(a);
}
