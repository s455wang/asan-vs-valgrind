#include <stdio.h>

struct container {
    int *x;
};

struct container container_new(void) {
    int a[5];
    struct container c = { a };
    printf("%p\n", &c);
    return c;
}

int main(void) {
    struct container c = container_new();
    c.x[2] = 5; // ASan catches here
}
